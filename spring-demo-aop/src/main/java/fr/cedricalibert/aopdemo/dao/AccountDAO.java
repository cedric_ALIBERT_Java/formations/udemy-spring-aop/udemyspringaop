package fr.cedricalibert.aopdemo.dao;

import org.springframework.stereotype.Component;

import fr.cedricalibert.aopdemo.Account;

@Component
public class AccountDAO {
	public void addAccount(Account account, boolean vip) {
		System.out.println(getClass()+"Doing my db work: adding an account");
	}
	
	public boolean doWork() {
		System.out.println(getClass()+"Doing work");
		return true;
	}
}
